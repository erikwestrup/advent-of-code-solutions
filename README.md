# Advent of Code Solutions 🎄
![Number of solved puzzles](https://img.shields.io/github/directory-file-count/erikw/advent-of-code-solutions?extension=txt)
![GitHub language count](https://img.shields.io/github/languages/count/erikw/advent-of-code-solutions)
![GitHub top language](https://img.shields.io/github/languages/top/erikw/advent-of-code-solutions)
[![SLOC](https://img.shields.io/tokei/lines/github/erikw/advent-of-code-solutions?logo=codefactor&logoColor=lightgrey)](#)
![GitHub last commit (branch)](https://img.shields.io/github/last-commit/erikw/advent-of-code-solutions/main)

My solutions to puzzles at [adventofcode.com](https://adventofcode.com/).

See also:
* [erikw/hackerrank-solutions](https://github.com/erikw/hackerrank-solutions/)
* [erikw/kattis-solutions](https://github.com/erikw/kattis-solutions/)
* [erikw/leetcode-solutions](https://github.com/erikw/leetcode-solutions/)
* [erikw/project-euler](https://github.com/erikw/project-euler)
